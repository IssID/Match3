// масив для удобной работы
var arrayChip = [[],[],[],[],[],[],[],[],[],[]];
// объявляем переменные что-бы обращаться к ним через консоль
var background, tile, chip_1, chip_2, chip_3, chip_4, chip_5, basicText, stage, score=0, end = 0;
// сколько набрать очков
var count = 200;

// ждем загрузки dom
document.addEventListener('DOMContentLoaded', function(){
    // скрываем версию pixi в консоли
    PIXI.utils.skipHello();

    // задаем настройки окна
    var app = PIXI.autoDetectRenderer(1024,768,{
        transparent: true,
        resolution: 1
    });
    // указываем где создать фрейм или канвас
    document.body.appendChild(app.view);
    stage = new PIXI.Container();

    // загружаем текстурки
    PIXI.loader
        .add("background", "src/img/background.png")
        .add("tile", "src/img/tile.png")
        .add("chip_1", "src/img/chip_1.png")
        .add("chip_2", "src/img/chip_2.png")
        .add("chip_3", "src/img/chip_3.png")
        .add("chip_4", "src/img/chip_4.png")
        .add("chip_5", "src/img/chip_5.png")
        .load(setup);

    // отрисовка объектов
    function setup(){
        background = new PIXI.Sprite(PIXI.loader.resources["background"].texture);
        tile = new PIXI.Sprite(PIXI.loader.resources["tile"].texture);
        chip_1 = new PIXI.Sprite(PIXI.loader.resources["chip_1"].texture);
        chip_2 = new PIXI.Sprite(PIXI.loader.resources["chip_2"].texture);
        chip_3 = new PIXI.Sprite(PIXI.loader.resources["chip_3"].texture);
        chip_4 = new PIXI.Sprite(PIXI.loader.resources["chip_4"].texture);
        chip_5 = new PIXI.Sprite(PIXI.loader.resources["chip_5"].texture);

        // создаем фон
        background.name = 'background';
        stage.addChild(background);

        // текст 
        basicText = new PIXI.Text('Ваш счет:');
        basicText.x = 0;
        basicText.y = 0;
        basicText.name = 'basicText';
        stage.addChild(basicText);

        basicText = new PIXI.Text('наберите '+count+' очков');
        basicText.x = ((app.width) - basicText.width);
        basicText.y = 0;
        basicText.name = 'basicText';
        stage.addChild(basicText);

        // создаем сетку 10x10 из клеток
        for (var i = 0; i < 100; i++) {
            var tiles = new PIXI.Sprite(PIXI.loader.resources["tile"].texture);
            tiles.x = ((app.width - tiles.width*10)/ 2)+ (i % 10) * 70;
            tiles.y = ((app.height - tiles.height*10)/ 1.7) + Math.floor(i / 10) * 70;
            tiles.name = 'tiles';
            stage.addChild(tiles);
        }

        // гейм луп
        animationLoop();
    }

    // основной цикл игры
    function animationLoop(){
        requestAnimationFrame(animationLoop);
        // прекращает отрисовку если игра закончилась
        if(!end){
            // отображаем счет
            scores(score);
            // создаем и двигаем chips
            createChip();
            // // получаем расположение мышки
            checkMouse();
            // перемещение chip по клику
            clickToMove();
            // проверяем рядом находящиеся chip
            checkDask();
        }
        // отрисовываем все объекты
        if(score >= count){
            theEnd();
        }
        app.render(stage);
    }

    function scores(req){
        // отображаем счет
        if(req){
            // чистим старое значение
            for(var i = 0; i < stage.children.length; i++){
                if(stage.children[i].name == 'score'){
                    stage.children[i].destroy();
                }
            }
            // отображаем новое
            basicText = new PIXI.Text(req);
            basicText.x = 125;
            basicText.y = 0;
            basicText.name = 'score';
            stage.addChild(basicText);
        }
    }
    // создаем первый ряд chips
    var id = 0;
    function createChip(){
        var corX=0,corY=0;
        var x = 0;
        var y = 0;
        var arrChip = ["chip_1","chip_2","chip_3","chip_4","chip_5"];
        for(var i = 0; i <= 9; i++){
            if(!arrayChip[0][i]){
                var chipName = arrChip[getRandomInt(5)];
                var chip = new PIXI.Sprite(PIXI.loader.resources[chipName].texture);
                chip.x = ((app.width - chip.width*10)/ 2.5) + (i % 10) * 70;
                chip.x = chip.x - 3.6;
                chip.y = ((app.height - chip.height*10)/4) + Math.floor(i / 10) * 70;
                chip.y = chip.y + 3;
                chip.chipName = chipName;
                chip.id = id;
                id++;
                chip.onClickAdd = 0;
                chip.name = 'chip';
                chip.corY = 0;
                chip.corX = i;

                var chiper = stage.addChild(chip);
                arrayChip[0][i] = chiper;

                // перемещение
                subscribe(chip);
            }
        }
        // двигаем chip вниз если ниже свободно
        for(var yY = 1; yY < 10; yY++){
            for(var xX = 0; xX < 10; xX++){
                if(!arrayChip[yY][xX]){
                    if(arrayChip[yY-1]){
                        var idd = arrayChip[yY-1][xX].id;
                        arrayChip[yY][xX] = arrayChip[yY-1][xX];
                        arrayChip[yY][xX].corY = arrayChip[yY-1][xX].corY + 1;
                        arrayChip[yY][xX].y = arrayChip[yY-1][xX].y + 70;
                        arrayChip[yY-1][xX] = 0;
                    }
                }
            }
        }
    }

    // удаляем и двигаем chips
    function checkDask(){
        for(var y = 0; y < arrayChip.length; y++){
            for(var x = 0; x < arrayChip.length; x++){
                // проверяем совпадения по оси x
                if(arrayChip[y][x+1] && arrayChip[y][x-1]){
                    if(arrayChip[y][x].chipName == arrayChip[y][x+1].chipName && arrayChip[y][x].chipName == arrayChip[y][x-1].chipName){
                        arrayChip[y][x].scale.x = 0.5;
                        arrayChip[y][x].scale.y = 0.5;
                        arrayChip[y][x-1].scale.x = 0.5;
                        arrayChip[y][x-1].scale.y = 0.5;
                        arrayChip[y][x+1].scale.x = 0.5;
                        arrayChip[y][x+1].scale.y = 0.5;
                    }
                }
                // проверяем совпадения по оси y
                if(arrayChip[y+1] && arrayChip[y-1]){
                    if(arrayChip[y+1][x] && arrayChip[y-1][x]){
                        if(arrayChip[y][x].chipName == arrayChip[y+1][x].chipName && arrayChip[y][x].chipName == arrayChip[y-1][x].chipName){
                            arrayChip[y][x].scale.x = 0.5;
                            arrayChip[y][x].scale.y = 0.5;
                            arrayChip[y-1][x].scale.x = 0.5;
                            arrayChip[y-1][x].scale.y = 0.5;
                            arrayChip[y+1][x].scale.x = 0.5;
                            arrayChip[y+1][x].scale.y = 0.5;
                        }
                    }
                }
            }
        }

        // отключаем выделение если выделили 2 chip
        for(var i = 0; i < stage.children.length; i++){
            if(stage.children[i].onClickAdd){
                for(var j = 0; j < stage.children.length; j++){
                    if(stage.children[j].onClickAdd && i != j){
                        stage.children[i].onClickAdd = 0;
                        stage.children[j].onClickAdd = 0;
                        ofDragStart(stage.children[i]);
                        ofDragStart(stage.children[j]);
                    }
                }
            }
        }

        // удаляем совпадения в реду по scale координата x (можно заменить на свое значение)
        for(var i = 0; i < stage.children.length; i++){
            if(stage.children[i].scale._x == 0.5){
                delete arrayChip[stage.children[i].corY][stage.children[i].corX];
                stage.children[i].destroy();
                score++;
            }
        }
    }

    // координаты мышки
    function checkMouse(){
            document.onmousedown = function(e){
                for(var i=0; i< stage.children.length; i++){
                    if(e.clientX > stage.children[i].x && e.clientX < stage.children[i].x+70 && e.clientY > stage.children[i].y && e.clientY < stage.children[i].y+70){
                        if(stage.children[i].name == 'chip'){
                            // // удалить chip (для тестирования) (точные координаты e.clientX e.clientY)
                            // delete arrayChip[stage.children[i].corY][stage.children[i].corX];
                            // stage.children[i].destroy();
                            // console.log(arrayChip[stage.children[i].corY][stage.children[i].corX].corY+" "+arrayChip[stage.children[i].corY][stage.children[i].corX].corX+" | "+stage.children[i].corY+" : "+stage.children[i].corX+" children "+i+" id "+stage.children[i].id+" ids "+arrayChip[stage.children[i].corY][stage.children[i].corX].id);
                        }
                    }
                }
            }
    }

    // перемещение по клику мышки
    function clickToMove(){
        for(var y = 0; y < arrayChip.length; y++){
            for(var x = 0; x < arrayChip[y].length; x++){
                if(arrayChip[y][x]){
                    if(arrayChip[y-1]){
                        if(arrayChip[y-1][x] && arrayChip[y][x].onClickAdd){
                            if(arrayChip[y][x].onClickAdd == arrayChip[y-1][x].onClickAdd){
                                var z = y -1;
                                moveY(y,x,z);
                            }
                        }
                    }
                    if(arrayChip[y][x-1]){
                        if(arrayChip[y][x-1] && arrayChip[y][x].onClickAdd){
                            if(arrayChip[y][x].onClickAdd == arrayChip[y][x-1].onClickAdd){
                                var z = x -1;
                                moveX(y,x,z);
                            }
                        }
                    }
                }
            }
        }
    }
    function moveX(y,x,z){
        var aX = arrayChip[y][z].x;
        var aaX = arrayChip[y][z];
        var aCorX = arrayChip[y][z].corX;

        var bX = arrayChip[y][x].x;
        var bbX = arrayChip[y][x];
        var bCorX = arrayChip[y][x].corX;

        arrayChip[y][x].x = aX;
        arrayChip[y][x].corX = aCorX;
        arrayChip[y][x] = aaX;

        arrayChip[y][z].x = bX;
        arrayChip[y][z].corX = bCorX;
        arrayChip[y][z] = bbX;

        arrayChip[y][x].onClickAdd = 0;
        arrayChip[y][z].onClickAdd = 0;

        ofDragStart(arrayChip[y][x]);
        ofDragStart(arrayChip[y][z]);
    }
    function moveY(y,x,z){
        var aY = arrayChip[z][x].y;
        var aaY = arrayChip[z][x];
        var aCorY = arrayChip[z][x].corY;

        var bY = arrayChip[y][x].y;
        var bbY = arrayChip[y][x];
        var bCorY = arrayChip[y][x].corY;

        arrayChip[y][x].y = aY;
        arrayChip[y][x].corY = aCorY;
        arrayChip[y][x] = aaY;

        arrayChip[z][x].y = bY;
        arrayChip[z][x].corY = bCorY;
        arrayChip[z][x] = bbY;

        arrayChip[y][x].onClickAdd = 0;
        arrayChip[z][x].onClickAdd = 0;

        ofDragStart(arrayChip[y][x]);
        ofDragStart(arrayChip[z][x]);
    }

    // получаем рандомное число от нуля до указанного
    function getRandomInt(max)
    {
        return Math.floor(Math.random() * max);
    }

    // нажатие на chip
    function subscribe(obj) {
        obj.interactive = true;
        obj.on('mousedown', onDragStart)
            .on('touchstart', onDragStart)
            .on('mouseup', onDragEnd)
            .on('mouseupoutside', onDragEnd)
            .on('touchend', onDragEnd)
            .on('touchendoutside', onDragEnd);
    }
    // делаем прозрачным и отмечаем нажатие
    function onDragStart(event) {
        if (!this.dragging) {
            this.data = event.data;
            this.dragging = true;
            if(!this.onClickAdd){
                this.alpha = 0.5;
                this.onClickAdd = 1;
            }else{
                this.alpha = 1;
                this.onClickAdd = 0;
            }
            this.dragPoint = event.data.getLocalPosition(this.parent);
            this.dragPoint.x -= this.x;
            this.dragPoint.y -= this.y;
        }
        // console.log(this.chipName+" "+this.corX+" "+this.corY+" | "+this.onClickAdd+" "+this.y);
    }
    function onDragEnd() {
        if (this.dragging) {
            this.dragging = false;
            this.data = null;
        }
    }
    // снимаем прозрачность
    function ofDragStart(obj){
        obj.alpha = 1;
    }
    // экоан конец игры

    function theEnd(){
        // создаем фон
        if(!end){
            background.name = 'background';
            stage.addChild(background);
            basicText = new PIXI.Text('Конец игры',{
                fontWeight: 'bold',
                fontSize: 60,
                fontFamily: 'Arial',
                fill: '#cc00ff',
                align: 'center',
                stroke: '#FFFFFF',
                strokeThickness: 6,
                name: 'score'

            });
            basicText.x = ((app.width/2) - basicText.width/2);
            basicText.y = ((app.height/2.5) - basicText.height/2);
            stage.addChild(basicText);
            end = 1;
        }
    }
});